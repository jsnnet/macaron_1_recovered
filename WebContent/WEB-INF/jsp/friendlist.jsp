<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<br>
<br>
<br>
<br>
<br>
<br>

<div class="content">

	<style>
#list {
	color: black;
	border: solid 1px;
	text-align: center;
	width: 800px;
	/* 	margin-left: 350px; */
	margin: auto;
}

#store_list {
	/* 	border: 10px solid transparent; */
	text-align: center;
	table-layout: fixed;
	/* border: 1px solid; */
}

#td_menu {
	vertical-align: bottom;
}

#menu_modal {
	/* width: auto; */
	height: 150px;
}
</style>
	<div id="list">

		<fieldset style="border: 1px solid rgba(255, 255, 255, .5);"
			id="container">
			<a style="height: 300px">${sessionScope.uname}님의 친구목록인데 넌없음ㅅㄱ</a>
			<table style="width: 798px; height: 300px;">
				<tr>
					<td>
						<button id="menu_modal" class="btn btn-default"
							data-target="#layerpop_list" data-toggle="modal">친구리스트인대
							넌친구 없잔아 멍청아
							</button>
					</td>
					<td>
						<button id="menu_modal" class="btn btn-default"
							data-target="#layerpop_search" data-toggle="modal">친구
							찾기인대 넌친구 없잔아 멍청아</button>
					</td>
					<td>
						<button id="menu_modal" class="btn btn-default"
							data-target="#layerpop_access" data-toggle="modal">친구
							수락인대 너한태걸놈 없음ㅅㄱ</button>
					</td>
				</tr>
			</table>
		</fieldset>
		<div class="row" id="menudiv"></div>
	</div>

	<div class="modal fade" id="layerpop_list">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Header</h4>
				</div>
				<div class="modal-body">
					<legend>${sessionScope.uname}님의 친구수락목록인데 넌친구 없잔아</legend>
					<table>
						<c:forEach var="e" items="${list}">
							<tr>
								<td>${e.friendid}</td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<div class="modal-footer">
					Footer
					<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				</div>
			</div>
		</div>
	</div>
</div>


<!--  -->


<div class="modal fade" id="layerpop_search">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Header</h4>
			</div>
			<div class="modal-body">
				<legend>친구 찾기</legend>
				<form method="post" action="upsave" autocomplete="off"
					enctype="multipart/form-data">
					<input type="hidden" name="stnum" id="stnum" value="">
					<p>
						<label>친구 아이디 검색</label> 
						<input type="text" name="foodname" id="foodname"> 
					</p>
					<p>
						<label>음식 가격</label> <input type="text" name="foodpay"
							id="foodpay">
					</p>
					<p>
						<label>음식 사진</label> <input type="file" name="upimg" id="upimg">
					</p>
					<p style="text-align: right;">
						<input type="submit" value="등록">
					</p>
				</form>
			</div>
			<div class="modal-footer">
				Footer
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>



<!--  -->



<div class="modal fade" id="layerpop_access">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Header</h4>
			</div>
			<div class="modal-body">
				<legend>친구수락</legend>
				<form method="post" action="upsave" autocomplete="off"
					enctype="multipart/form-data">
					<input type="hidden" name="stnum" id="stnum" value="">
					<p>
						<label>음식이름</label> <input type="text" name="foodname"
							id="foodname">
					</p>
					<p>
						<label>음식 가격</label> <input type="text" name="foodpay"
							id="foodpay">
					</p>
					<p>
						<label>음식 사진</label> <input type="file" name="upimg" id="upimg">
					</p>
					<p style="text-align: right;">
						<input type="submit" value="등록">
					</p>
				</form>
			</div>
			<div class="modal-footer">
				Footer
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>


<%@include file="footer.jsp"%>