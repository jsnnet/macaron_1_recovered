package kr.co.macaronshop.mvc.dao;

import java.util.List;


import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.BasketListDTO;
import kr.co.macaronshop.mvc.dto.BasketVO;

@Repository
public class BasketDao {
	
	@Autowired
	private SqlSessionTemplate ss;
	
	// 장바구니 리스트
	public List<BasketListDTO> listBasket2(int memnum){
		return ss.selectList("cart.listBasket2", memnum);
	} 
	
	// 가게의 상품 골라서 장바구니로 추가
	public void intoCart(BasketVO vo) {
		ss.insert("cart.addBasket", vo);
	}
	
	// 장바구니 리스트에서 삭제 눌러서 개별 삭제
	public void delCart(int basketnum) {
		ss.delete("cart.delCart", basketnum);
	}

	// 장바구니에 들어온 전체 상품 가격 
	public BasketListDTO totalPrice(int memnum) {
		return ss.selectOne("cart.totalPrice", memnum);
	}
	
	// 체크 선택 후 구매하면 결제 테이블에 상품 정보 추가하기
	public BasketVO cartPay(int basketnum) {
		return ss.selectOne("cart.cartpay", basketnum);
	}

	// 장바구니 구매 버튼 후 결제 내역으로 넣기
	public void insertPay(BasketVO pvo) {
		ss.insert("cart.insertpay", pvo);
	}
}
