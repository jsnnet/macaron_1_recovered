package kr.co.macaronshop.mvc.dao;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.Dutch_PayVO;
import kr.co.macaronshop.mvc.dto.FriendVO;
import kr.co.macaronshop.mvc.dto.MemberVO;

@Repository
public class MyPageDao {
	@Autowired
	private SqlSessionTemplate ss;
	public FriendVO getChk(FriendVO fvo) {
		return 	ss.selectOne("mypage.friendchk",fvo);
	}
	//친구 신청
	public void getAddFriend(FriendVO fvo) {
		ss.insert("mypage.addFriend",fvo);
	}
	
	//친구 신청 받은 수
	public List<String> getFriendWait(String memid){
		return ss.selectList("mypage.friendWait",memid);
	}
	
	//양쪽으로 신청 받은 수
	public List<String> getfriendList(String memid) {
		List<FriendVO> list = ss.selectList("mypage.friendlist", memid);
		List<String> friendList = new ArrayList<>();
		//내가 friend에 있을 수도 있기 때문에 한쪽으로 정렬 시킴
		for(FriendVO e : list) {
			if(!e.getFriendid().equals(memid)) {
				friendList.add(e.getFriendid());
			}else if(!e.getMemid().equals(memid)) {
				friendList.add(e.getMemid());
			}
		}
		return friendList;
	}
	
	//친구를 건 인원수 
	public int getWaitCnt(String memid) {
		return ss.selectOne("mypage.waitCnt",memid);
	}
	//친구 신청 한 인원 수 getWaitCnt_apply
	public int getWaitCnt_apply(String memid) {
		return ss.selectOne("mypage.getWaitCnt_apply",memid);
	}
	
	//더치페이 현황 카운터
	public int getWaitCnt_Dutch(String memid) {
		return ss.selectOne("mypage.waitcnt_dutch",memid);
	}
	
	
	
	//친구 등록
	public void getFriendaccess(FriendVO fvo) {
		ss.insert("mypage.access",fvo);
	}
	
	//친구 거절
	public void getFrienddeny(FriendVO fvo) {
		ss.delete("mypage.deny",fvo);
	}
	
	//친구 신청
	public List<String> getFriendApplyList(String memid){
		return ss.selectList("mypage.friendApplyList",memid);
	}
	
	//맴버 정보 받기 이름 나이 주소
	public MemberVO getMeminfor(String memid) {
		return ss.selectOne("mypage.meminfor",memid);
	}
	
	//마이페이지의 더치 현황 리스트
	public List<Dutch_PayVO> getDutchlist(String memid) {
		return  ss.selectList("mypage.dutchlist" , memid);
	}
	
	
	
}
