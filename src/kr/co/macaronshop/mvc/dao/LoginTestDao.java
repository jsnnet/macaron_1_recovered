package kr.co.macaronshop.mvc.dao;

import kr.co.macaronshop.mvc.dto.MemberVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Repository
public class LoginTestDao {
	@Autowired
	private SqlSessionTemplate ss;
	
	public MemberVO getId(String memid) {
		return ss.selectOne("cart.getMem", memid);
	}

}
