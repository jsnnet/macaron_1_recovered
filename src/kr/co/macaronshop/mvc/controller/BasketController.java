package kr.co.macaronshop.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.BasketDao;
import kr.co.macaronshop.mvc.dto.BasketListDTO;
import kr.co.macaronshop.mvc.dto.BasketVO;
import kr.co.macaronshop.mvc.dto.PaymentVO;

@Controller
public class BasketController {

	@Autowired
	private BasketDao basketDao;


	// 장바구니 추가
	@RequestMapping(value = "/intoBasket", method = RequestMethod.POST)
	public ModelAndView intoBasket(@ModelAttribute("vo") BasketVO vo, HttpSession session, String category) {
		int memnum = (int) session.getAttribute("unum");
		System.out.println("(insert)세션의 회원번호 : " + memnum);
		System.out.println("카테고리 넘어오나? : "+category);
		vo.setMemnum(memnum);
		basketDao.intoCart(vo);
		ModelAndView mav = new ModelAndView("redirect:/listBasket2");
		return mav;
	}

	// 장바구니 보여주기 (select)
	@RequestMapping(value = "/listBasket2")
	public ModelAndView listBasket2(HttpSession session, BasketVO vo) {
		int memnum = (int) session.getAttribute("unum"); // 세션에 있던 회원번호(unum)을 받아 이용하기
		System.out.println("(컨트롤러)세션에 있던 회원번호 2 : " + memnum);
		System.out.println("카테고리 받아오기 : "+vo.getCategory());
		PaymentVO pvo = new PaymentVO();
		System.out.println("dok 저장된건가? : " + pvo.getDok());
		ModelAndView mav = new ModelAndView("basketlist2");
		List<BasketListDTO> dtolist = basketDao.listBasket2(memnum);
		System.out.println("장바구니 목록 : " + dtolist);
		BasketListDTO tp = basketDao.totalPrice(memnum);// 장바구니에 담긴 모든 상품에 대한 주문가격의 합계
		mav.addObject("tp", tp);
		mav.addObject("dtolist", dtolist);
		return mav;
	}

	// 개별 삭제
	@GetMapping(value = "/deleteselected")
	public String delCart(int basketnum) {
		basketDao.delCart(basketnum);
		return "redirect:/listBasket2";
	}

	// 장바구니에서 구매 버튼 클릭 그리고 구매된 항목들은 장바구니에서 제거
	@RequestMapping(value = "/clickPay", method = RequestMethod.POST)
	public ModelAndView clickPay(@RequestParam(value = "chk[]") int[] chbox, HttpSession sesstion, boolean check) {
		ModelAndView mav = new ModelAndView("redirect:/storelistController?page=1");
		BasketVO ppvo = new BasketVO();
		for (int e : chbox) {
			ppvo = basketDao.cartPay(e);
			basketDao.insertPay(ppvo);
			System.out.println("장바구니 번호 : " + e);
			basketDao.delCart(e);
		}
		return mav;
	}
}
