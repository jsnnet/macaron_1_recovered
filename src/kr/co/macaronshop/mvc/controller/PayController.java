package kr.co.macaronshop.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.MyPageDao;
import kr.co.macaronshop.mvc.dao.PaymentDao;
import kr.co.macaronshop.mvc.dto.Dutch_PayVO;
import kr.co.macaronshop.mvc.dto.PaymentVO;

@Controller
public class PayController {
	@Autowired
	private PaymentDao paymentDao;
	@Autowired
	private MyPageDao myPageDao;

	@RequestMapping("pay")
	public ModelAndView pay(PaymentVO pvo) {
		ModelAndView mav = new ModelAndView("pay");
		mav.addObject("msg", "결제 페이지 입니다.");
		mav.addObject("pvo", pvo);
		return mav;
	}

	@RequestMapping("payment")
	public String paypay(PaymentVO pvo, HttpSession session) {
		System.out.println((int) session.getAttribute("unum"));
		pvo.setMemnum((int) session.getAttribute("unum"));
		paymentDao.getInsert(pvo);

		return "hi";
	}

	@RequestMapping("dutch_pay")
	public ModelAndView duch_pay(PaymentVO pvo, HttpSession session) {
		// 확인 더치페이 진행 중인지 확인
		// 결제 미완료면 pay page로 이동하고
		int memnum = (int) session.getAttribute("unum");
		String memid = (String) session.getAttribute("uid");
		int pro_status = paymentDao.getProStatus(memnum);
		int count_friend = paymentDao.getCountFriend(memid);
		if (count_friend == 0) {
			ModelAndView mav = new ModelAndView("pay");
			mav.addObject("msg", "넌 친구 없어");
			mav.addObject("pvo", pvo);
			return mav;
		} else if (pro_status == 1) {
			ModelAndView mav = new ModelAndView("pay");
			mav.addObject("msg", "결제 대기중인 더치페이가 있습니다");
			mav.addObject("pvo", pvo);
			return mav;
		} else {

			// dutch 결제가 없으면 dutch_pay시작
			ModelAndView mav = new ModelAndView("dutch_pay");
			// 친구리스트
			List<String> friend_list = myPageDao.getfriendList(memid);
			mav.addObject("friend_list", friend_list);
			mav.addObject("pvo", pvo);
			return mav;
		}

	}

	// duch_pro결제대기
	@RequestMapping("dutch_pro")
	public String duch_pro(PaymentVO pvo, HttpSession session, Dutch_PayVO dvo) {
		if(dvo.getPay().equals("")) {
			dvo.setPay1(0);
		}else {
			dvo.setPay1(Integer.parseInt(dvo.getPay()));
		}
		pvo.setMemnum((int) session.getAttribute("unum"));
		dvo.setMemid1((String) session.getAttribute("uid"));
		// 결제대기 -> 결제 테이블에 들어감
		paymentDao.setPaymentDutchInsert(pvo);
		// 더치 테이블에 넣기
		dvo.setPay_num(paymentDao.getPayNum(pvo.getMemnum()));
		paymentDao.setDutchInsert(dvo);
		
		return "redirect:myPage";
	}

	// dutch_update 값을 수정
	@RequestMapping("dutch_update")
	public String dutch_update(Dutch_PayVO dvo, HttpSession session) {

		if (dvo.getPay().equals("")) {
			return "redirect:dutch_status";
		} else {
			//int pay1 = Integer.parseInt(dvo.getPay());
			String memid = (String) session.getAttribute("uid");
			dvo.setPay1(Integer.parseInt(dvo.getPay()));
			dvo.setMemid(memid);
			paymentDao.setDutchUpdate(dvo);
			return "redirect:dutch_status";
		}
	}
	
	//dutch_delete 거래 취소
	@RequestMapping("dutch_delete")
	public String dutch_delete(String name) {
		return "myPage";
	}
}
